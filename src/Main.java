public class Main {
    public static void main(String[] args) {
        Huffman huffman = new Huffman();
        String code = huffman.encode("ABRACADABRA", true);
        System.out.println(code);
        System.out.println(huffman.decode(code));
        huffman.dumpPrefixCodes(true);
        huffman.dumpPrefixCodes(false);
    }
}
